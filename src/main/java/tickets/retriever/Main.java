package tickets.retriever;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import tickets.retriever.model.TicketUrl;
import tickets.retriever.services.scan.ScanService;
import tickets.retriever.utils.ExcelUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static tickets.retriever.model.Constants.BASE_URL;
import static tickets.retriever.model.Constants.BASE_URL_DOMAIN;

/**
 * the main class the will start the process
 */
public class Main {

	private static final Logger LOG = Logger.getLogger(Main.class);

	public static void main(String[] args){
		Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
		Logger.getLogger("org.apache").setLevel(Level.OFF);
		LOG.info("Search started!");
		ScanService scanService = new ScanService();
		Set<String> urls = new HashSet<>();
		List<TicketUrl> result = new ArrayList<>();
		scanService.scanWebsite(BASE_URL, BASE_URL_DOMAIN, urls, result);

		ExcelUtils.exportUrlsToExcelDocuments(result);
		LOG.info(String.format("Search completed, target urls found: %d!", result.size()));
	}

}
