package tickets.retriever.model;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * stores shared java constants
 */
public class Constants
{
	private Constants(){

	}
	public static final String BASE_URL = "http://www.box-officetickets.com/Cityguides";
	public static final String BASE_URL_DOMAIN = "box-officetickets.com";
	public static final List<String> REQUIRED_URLS_SUBSTRINGS = Lists.newArrayList("/events/", "/event/", "/venue/");
	//todo: change for prod
	public static final int MAXIMUM_NUMBER_OF_URLS = 200000;

	public static final String OUTPUT_FILE_NAME = "Urls-";

	public static final String NO_TICKETS_TEXT_CONDITION = "\"Tickets\":[]";
	public static final String HTTP_PREFIX = "http://";
	public static final String LINK = "a";
	public static final String AREA_LINK = "area";

	public static final int MAX_ROWS_FILE = 50000;
	public static final String DEFAULT_SHEET_NAME = "URLS";
	public static final String FILE_EXT = ".xls";
	public static final String HAS_TICKETS_TRUE = "YES";
	public static final String HAS_TICKETS_FALSE = "NO";
	public static final String URL_COL_NAME = "URL";
	public static final String HAS_TICKETS_CAL_NAME = "Has Tickets";
	public static final String LIMIT = "limit=";
	public static final String PAGINATION_LINK = "/atbs_ajax/events?";


}
