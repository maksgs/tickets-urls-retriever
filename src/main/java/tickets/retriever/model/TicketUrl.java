package tickets.retriever.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * represent ticket url
 */
@AllArgsConstructor
@ToString
public class TicketUrl {

	@Getter
	@Setter
	private String url;

	@Getter
	@Setter
	private Boolean hasTickets;

}
