package tickets.retriever.services;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import tickets.retriever.model.Constants;
import tickets.retriever.model.TicketUrl;
import tickets.retriever.utils.SearchUtils;
import tickets.retriever.utils.URLUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * this webservice will help to return the content of the web page
 */
public class UrlService {

	private static final Logger LOG = Logger.getLogger(UrlService.class);

	/**
	 * recursive method that get page content and tries to find contents of page links
	 * @param url - page url
	 * @param domain - domain of the website
	 * @param scannedUrls - set of scanned urls
	 * @param targetUrls - list of target urls
	 */
	public void scanWebsitePage(String url, String domain, Set<String> scannedUrls,
								List<TicketUrl> targetUrls, int step) {
		if (url.contains(domain) && !scannedUrls.contains(url) && scannedUrls.size() < Constants.MAXIMUM_NUMBER_OF_URLS){
			scannedUrls.add(url);

			Document doc = URLUtils.getDocument(url);
			if (doc == null){
				return;
			}
			if (Constants.REQUIRED_URLS_SUBSTRINGS.stream()
					.filter(item-> url.toLowerCase().contains(item))
					.collect(Collectors.toList()).size() > 0){
				TicketUrl ticketUrl = new TicketUrl(url, !doc.html().contains(Constants.NO_TICKETS_TEXT_CONDITION));
				targetUrls.add(ticketUrl);
			}

			/**
			 * log about each 200 urls were processed
			 */
			if (scannedUrls.size() % 50 == 0 ){
				LOG.info(String.format("%d pages for %s were retrieved", scannedUrls.size(), domain));
			}

			//add content of all linked links if step is not big
			Set<String> links = SearchUtils.getInternalLinks(doc, domain);
			List<String> pagesLinks = links.stream()
					.filter(item-> item.contains(Constants.PAGINATION_LINK))
					.collect(Collectors.toList());
			if(!pagesLinks.isEmpty()){
				String link = pagesLinks.get(0);
				int start =  link.indexOf(Constants.LIMIT);
				if (start > 0){
					links.add(String.format("%s%s", link.substring(0,start), "limit=20000&offset=0"));
				}
			}
			links.removeAll(scannedUrls);
			doc = null;
			links.stream().forEach(link ->
					scanWebsitePage(link, domain, scannedUrls, targetUrls, step + 1 )
			);
		}
	}

}
