package tickets.retriever.services.scan;

import org.apache.log4j.Logger;
import tickets.retriever.model.TicketUrl;
import tickets.retriever.services.UrlService;

import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

public class ScanPagesJob implements Callable<Integer> {

	private static final Logger LOG = Logger.getLogger(ScanPagesJob.class);

	private UrlService urlService;
	private String link;
	private String domain;
	private Set<String> scannedUrls;
	private List<TicketUrl> ticketUrls;

	public ScanPagesJob(UrlService urlService, String link, String domain,
						Set<String> scannedUrls, List<TicketUrl> ticketUrls) {
		this.urlService = urlService;
		this.link = link;
		this.domain = domain;
		this.scannedUrls = scannedUrls;
		this.ticketUrls = ticketUrls;
	}

	@Override
	public Integer call() {
		LOG.info(String.format("Scan job started for %s", link));
		urlService.scanWebsitePage(link, domain, scannedUrls, ticketUrls, 1);
		LOG.info(String.format("Scan job completed for %s", link));
		return 0;
	}


}
