package tickets.retriever.services.scan;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import tickets.retriever.model.TicketUrl;
import tickets.retriever.services.UrlService;
import tickets.retriever.utils.SearchUtils;
import tickets.retriever.utils.URLUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * scans page content
 */
public class ScanService {

	private static final Logger LOG = Logger.getLogger(ScanService.class);

	private UrlService urlService = new UrlService();

	/**
	 * recursive method that get page content and tries to find contents of page links
	 * @param url - page url
	 * @param domain - domain of the website
	 * @param scannedUrls - set with scanned urls
	 */
	public void scanWebsite(String url, String domain, Set<String> scannedUrls, List<TicketUrl> result) {
		Document doc = URLUtils.getDocument(url);
		if (doc == null){
			return;
		}

		//create callable task for each url
		Set<String> links = SearchUtils.getAreaLinks(doc, domain);

		/**
		 * run pages scanning in different threads
		 */
		final Collection<Future<Integer>> futures = new ArrayList<>();
		ExecutorService executorService = Executors.newFixedThreadPool(links.size());
		final ExecutorCompletionService<Integer> completionService = new ExecutorCompletionService<>(executorService);

		links.stream().forEach(link ->
				futures.add(completionService.submit(new ScanPagesJob(urlService, link, domain, scannedUrls, result)))
		);
		int sum = 0;
		try{
			for(int i= 0; i < futures.size(); i++){
				sum += completionService.take().get();
			}
		}catch (ExecutionException | InterruptedException ex){
			LOG.info(String.format("Error during scanning website %s, error: %s", url, ex.getMessage()));
		}
		LOG.info("Executors were invoked!");
		executorService.shutdownNow();

	}

}
