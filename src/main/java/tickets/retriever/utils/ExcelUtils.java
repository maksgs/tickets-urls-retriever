package tickets.retriever.utils;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import tickets.retriever.model.TicketUrl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static tickets.retriever.model.Constants.DEFAULT_SHEET_NAME;
import static tickets.retriever.model.Constants.FILE_EXT;
import static tickets.retriever.model.Constants.HAS_TICKETS_CAL_NAME;
import static tickets.retriever.model.Constants.HAS_TICKETS_FALSE;
import static tickets.retriever.model.Constants.HAS_TICKETS_TRUE;
import static tickets.retriever.model.Constants.MAX_ROWS_FILE;
import static tickets.retriever.model.Constants.OUTPUT_FILE_NAME;
import static tickets.retriever.model.Constants.URL_COL_NAME;

public class ExcelUtils {

	private static final Logger LOG = Logger.getLogger(ExcelUtils.class);

	/**
	 * Exports urls into excel documents
	 *
	 * @param urls
	 *            list of TicketUrl
	 */
	public static void exportUrlsToExcelDocuments(final List<TicketUrl> urls) {
		LOG.info(String.format("exportUrlsToExcelDocuments invoked with [urls.size: %d]", urls.size()));
		int filesCount = urls.size() / MAX_ROWS_FILE;
		if (urls.size() % MAX_ROWS_FILE != 0) {
			filesCount++;
		}
		for (int i = 0; i < filesCount; i++) {
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(DEFAULT_SHEET_NAME);
			int rowNum = 0;
			createRow(sheet, rowNum, URL_COL_NAME, HAS_TICKETS_CAL_NAME);
			for (int j = i * MAX_ROWS_FILE; j < (i + 1) * MAX_ROWS_FILE && j < urls.size(); j++) {
				String hasTicketLabel = urls.get(j).getHasTickets() ? HAS_TICKETS_TRUE : HAS_TICKETS_FALSE;
				createRow(sheet, ++rowNum, urls.get(j).getUrl(), hasTicketLabel);
			}
			String fileName = String.format("%s%d%s", OUTPUT_FILE_NAME, i, FILE_EXT);
			try {
				File file = new File(fileName);
				FileOutputStream out = new FileOutputStream(file);
				workbook.write(out);
				out.close();
				workbook.close();
				LOG.info(String.format("Excel file saved at: %s", file.getAbsolutePath()));
			} catch (IOException e) {
				LOG.info(String.format("Error during saving file: %s due %s", fileName, e.getMessage()));
			}
		}
	}

	private static void createRow(HSSFSheet sheet, int rowNum, String firstCol, String secondCol) {
		Row row = sheet.createRow(rowNum);
		Cell cell = row.createCell(0);
		cell.setCellValue(firstCol);
		cell = row.createCell(1);
		cell.setCellValue(secondCol);
	}
}
