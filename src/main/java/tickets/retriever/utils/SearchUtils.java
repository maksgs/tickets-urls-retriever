package tickets.retriever.utils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import tickets.retriever.model.Constants;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * util methods for search
 */
public class SearchUtils {


	public static Set<String> getAreaLinks(Document doc, String domain){
		List<Element> links = doc.select(Constants.AREA_LINK);
		return links.stream()
				.map(link -> URLUtils.getLinkUrl(link, domain))
				.filter(SearchUtils::isValidLink)
				.collect(Collectors.toSet());

	}

	public static Set<String> getInternalLinks(Document doc, String domain){
		List<Element> links = doc.select(Constants.LINK);
		return links.stream()
				.map(link -> URLUtils.getLinkUrl(link, domain))
				.filter(SearchUtils::isValidLink)
				.collect(Collectors.toSet());

	}

	/**
	 * check if link has valid href
	 * @param link - link
	 * @return true if link is valid
	 */
	private static boolean isValidLink(String link){
		return !(link.endsWith("#") || link.endsWith(".jpg") || link.endsWith(".png"));
	}
}
