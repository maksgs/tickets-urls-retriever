package tickets.retriever.utils;

import com.google.common.base.Strings;

/**
 * removes unused symbols from website html content
 */
public class SpecialCharactersEscaper
{
	private static final String SPECIAL_CHARACTERS = "(?=[]\\[+&|!(){}^\"~*?:\\\\/-])";
	private static final String SPECIAL_CHARACTERS_REPLACEMENT = "\\\\";

	private SpecialCharactersEscaper()
	{
	}

	/**
	 * return html content without skipped symbols
	 * @param source - source
	 * @return escaped content
	 */
	public static String escape(String source)
	{
		if(Strings.isNullOrEmpty(source)){
			return source;
		}
		return "\"" + source.replaceAll(SPECIAL_CHARACTERS, SPECIAL_CHARACTERS_REPLACEMENT) + "\"" ;
	}
}
