package tickets.retriever.utils;

import com.google.common.base.Strings;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import tickets.retriever.model.Constants;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * utils for url
 */
public class URLUtils {

	private static final String MOZILLA_AGENT = "Mozilla";

	private static final String WWW_PREFIX = "www.";

	private URLUtils() {
		//do nothing
	}

	/**
	 * returns url using jsoup Element link
	 * @param link - {@link org.jsoup.nodes.Element]
	 * @param domain - domain
	 * @return String
	 */
	public static String getLinkUrl(Element link, String domain){
		if (!Strings.isNullOrEmpty(link.attr("abs:href"))){
			return link.attr("abs:href");
		}
		if (domain == null || link.attr("href") == null){
			return null;
		}
		if (link.attr("href").startsWith("/")){
			return Constants.HTTP_PREFIX + WWW_PREFIX + domain + link.attr("href");
		}
		return Constants.HTTP_PREFIX + WWW_PREFIX + domain + "/" + link.attr("href");
	}

	/**
	 * get Jsoup document using url
	 * @param url - website url
	 * @return {@link org.jsoup.nodes.Document}
	 */
	public static Document getDocument(String url){
		try{
			//retrieve using http unit library
			return getPageContentUsingJsoup(url);
			//return getPageContentUsingHttpUnit(url, webClient);
		}catch (Exception ex){
			return null;
		}
	}

	/**
	 * process if url redirects to other url
	 * @param url - page url
	 * @return {@link Document}
	 * @throws java.io.IOException
	 */
	public static Document getPageContentUsingJsoup(String url) throws IOException {
		Connection.Response response = Jsoup.connect(url.replaceAll(" ", "%20"))
				.timeout(30 * 1000)
				.userAgent(MOZILLA_AGENT)
				.followRedirects(false)
				.execute();
		int status = response.statusCode();
		Document document = Jsoup.parse(response.body());
		Elements meta = document.select("html head meta");
		if (meta.attr("http-equiv").equalsIgnoreCase("refresh")){
			return getPageContentUsingJsoup(url + "/" + meta.attr("content").split("=")[1].trim());
		}
		if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)
		{
			String redirectUrl = response.header("location");
			return getPageContentUsingJsoup(redirectUrl);
		}
		return document;
	}

}
