1. Install Java 8 (JRE - Java SE Runtime Environment 8) http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
   or check your current java version by this command: java -version, it should return something like this: "java version "1.8.0_25"
2. Run application with next command: java -jar ticket-urls-retriever.jar [FILES_DIR]
   FILES_DIR - optional - is directory where files will be saved, if this parameter is absent "C:\" will be used as default.